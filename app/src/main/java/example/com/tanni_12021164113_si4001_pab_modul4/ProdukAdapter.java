package example.com.tanni_12021164113_si4001_pab_modul4;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.List;

/**
 * Created by tanni on 3/18/2018.
 */

public class ProdukAdapter extends ArrayAdapter<Makanan> {


    private Activity context;
    List<Makanan> produks;

    public ProdukAdapter(Activity context, List<Makanan> produks) {
        super(context, R.layout.item_layout_orang, produks);
        this.context = context;
        this.produks = produks;
    }

    //adapter makanan untuk di glide di listview pada main menu
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = context.getLayoutInflater();
        View listViewItem = inflater.inflate(R.layout.item_layout_orang, null, true);
        ImageView imageViewProduk = (ImageView) listViewItem.findViewById(R.id.imageView3);
        TextView textViewNama = (TextView) listViewItem.findViewById(R.id.namaMenu);
        TextView textViewNomor = (TextView) listViewItem.findViewById(R.id.hargaMenu);

        Makanan pd =  produks.get(position);

        Glide.with(context)
                .load(pd.getImage_url())
                .into(imageViewProduk );

        textViewNama.setText(pd.getNamaMenu());
        textViewNomor.setText("Rp."+pd.getHargaMenu());

        return listViewItem;
    }
}


