package example.com.tanni_12021164113_si4001_pab_modul4;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.ListView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = MainActivity.class.getSimpleName();
    ListView listViewProduks;
    DatabaseReference databaseProduk;
    Button btnLoad;
    List<Makanan> produks;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        btnLoad = findViewById(R.id.butnLoad);
        listViewProduks = (ListView) findViewById(R.id.listViewProduks);
        produks = new ArrayList<>();
        databaseProduk = FirebaseDatabase.getInstance().getReference("MenuMakanan");

        //listener button load makanan, akan mengeksekusi method MyTask dibawah yang
        //meng-extend AsyncTask
        btnLoad.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {

                new MyTask().execute();

            }
        });


        //listener floating action button untuk menambah makanan
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this,  TambahActivity.class));
            }
        });

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        //alert dialog ketika logout
        if (id == R.id.action_logout) {
            AlertDialog.Builder alert = new AlertDialog.Builder(MainActivity.this);
            alert.setTitle("KELUAR NIH");
            alert.setMessage("Yakin ingin keluar?")
                    //positive button -> eksekusi signout method firebase -> intent keluar
                    .setPositiveButton("Ya", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            Intent intent1 = new Intent(MainActivity.this,LoginActivity.class);
                            intent1.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(intent1);
                            //autentikasi signout firebase
                            FirebaseAuth.getInstance().signOut();

                            return;
                        }
                    })
                    //negative button -> tidak eksekusi apapun, kembali ke menu utama
                    .setNegativeButton("Tidak",null);
            alert.create();
            alert.show();
        }


        return super.onOptionsItemSelected(item);
    }

    //konten logout diinflate dari /res/menu/menu_main.xml
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        //layout konten logout diperoleh dari menu
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }



    private class MyTask extends AsyncTask<String, String, String> {

        @Override
        protected void onPreExecute() {

            super.onPreExecute();
        }


        @Override
        protected String doInBackground(String... params) {

            //load makanan menggunakan asynctask (pengambilan data dengan proses yang dilakukan
            //dibelakang proses/di background proses)
            databaseProduk.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {

                    produks.clear();
                    for (DataSnapshot produkSnapshot : dataSnapshot.getChildren()) {

                        Makanan produk = produkSnapshot.getValue(Makanan.class);
                        produks.add(produk);
                    }

                    ProdukAdapter produkAdapter = new ProdukAdapter(MainActivity.this, produks);
                    listViewProduks.setAdapter( produkAdapter);
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {
                    Log.e(TAG, "eror : .", databaseError.toException());

                }
            });

            return null;
        }

        @Override
        protected void onPostExecute(String s) {

            super.onPostExecute(s);
        }
    }
}