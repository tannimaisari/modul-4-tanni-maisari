package example.com.tanni_12021164113_si4001_pab_modul4;

/**
 * Created by tanni pc on 3/31/2019.
 */

public class Makanan {


    public Makanan(String id, String namaMenu, String hargaMenu, String deskripsiMenu, String image_url) {
        this.id = id;
        this.namaMenu = namaMenu;
        this.hargaMenu = hargaMenu;
        this.deskripsiMenu = deskripsiMenu;
        this.image_url = image_url;
    }

    public Makanan() {
    }

    //setter dan getter makanan

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNamaMenu() {
        return namaMenu;
    }

    public void setNamaMenu(String namaMenu) {
        this.namaMenu = namaMenu;
    }

    public String getHargaMenu() {
        return hargaMenu;
    }

    public void setHargaMenu(String hargaMenu) {
        this.hargaMenu = hargaMenu;
    }

    public String getDeskripsiMenu() {
        return deskripsiMenu;
    }

    public void setDeskripsiMenu(String deskripsiMenu) {
        this.deskripsiMenu = deskripsiMenu;
    }

    public String getImage_url() {
        return image_url;
    }

    public void setImage_url(String image_url) {
        this.image_url = image_url;
    }

    private String id;
    private String namaMenu;
    private String hargaMenu;
    private String deskripsiMenu;
    public String image_url;
}
